import random
import pygame
import time
import copy

pygame.font.init()

GAME_WIDTH = 600
GAME_HEIGHT = 700
PLAY_WIDTH = 300
PLAY_HEIGHT = 600
BLOCK_SIZE = 30

TOP_LEFT_X = 50
TOP_LEFT_Y = GAME_HEIGHT - PLAY_HEIGHT - 50

BOARD_ORIGINAL = [
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
]

Z = [
    [
        [0,0,0],
        [1,1,0],
        [0,1,1]
    ],
    [
        [0,1,0],
        [1,1,0],
        [1,0,0]
    ],
    [
        [1,1,0],
        [0,1,1],
        [0,0,0]
    ],
    [
        [0,0,1],
        [0,1,1],
        [0,1,0]
    ]
]

S = [
    [
        [0,0,0],
        [0,1,1],
        [1,1,0]
    ],
    [
        [1,0,0],
        [1,1,0],
        [0,1,0]
    ],
    [
        [0,1,1],
        [1,1,0],
        [0,0,0]
    ],
    [
        [0,1,0],
        [0,1,1],
        [0,0,1]
    ]
]

I = [
    [
        [0,0,0,0],
        [1,1,1,1],
        [0,0,0,0],
        [0,0,0,0]
    ],
    [
        [0,0,1,0],
        [0,0,1,0],
        [0,0,1,0],
        [0,0,1,0]
    ],
    [
        [0,0,0,0],
        [0,0,0,0],
        [1,1,1,1],
        [0,0,0,0]
    ],
    [
        [0,1,0,0],
        [0,1,0,0],
        [0,1,0,0],
        [0,1,0,0]
    ]
]

O = [
    [
        [0,0,0,0],
        [0,1,1,0],
        [0,1,1,0],
        [0,0,0,0]
    ]
]

J = [
    [
        [1,0,0],
        [1,1,1],
        [0,0,0]
    ],
    [
        [0,1,1],
        [0,1,0],
        [0,1,0]
    ],
    [
        [0,0,0],
        [1,1,1],
        [0,0,1]
    ],
    [
        [0,1,0],
        [0,1,0],
        [1,1,0]
    ]
]

L = [
    [
        [0,0,1],
        [1,1,1],
        [0,0,0]
    ],
    [
        [0,1,0],
        [0,1,0],
        [0,1,1]
    ],
    [
        [0,0,0],
        [1,1,1],
        [1,0,0]
    ],
    [
        [1,1,0],
        [0,1,0],
        [0,1,0]
    ]
]

T = [
    [
        [0,1,0],
        [1,1,1],
        [0,0,0]
    ],
    [
        [0,1,0],
        [0,1,1],
        [0,1,0]
    ],
    [
        [0,0,0],
        [1,1,1],
        [0,1,0]
    ],
    [
        [0,1,0],
        [1,1,0],
        [0,1,0]
    ]
]

SHAPES = [S, Z, I, O, J, L, T]
SHAPES_COLORS = [(0, 255, 0), (255, 0, 0), (0, 255, 255), (255, 255, 0),
                 (255, 165, 0), (0, 0, 255), (128, 0, 128)]

class Piece:
    def __init__(self, x, y, shape):
        self.x = x
        self.y = y
        self.shape = shape
        self.color = SHAPES_COLORS[SHAPES.index(shape)]
        self.rotation = 0


def add_piece(piece, board):
    shape = piece.shape[piece.rotation]
    for i in range(0, len(shape)):
        for j in range(0, len(shape[i])):
            if(shape[i][j] == 1):
                board[piece.y + i][piece.x + j] = SHAPES.index(piece.shape) + 1
    return board    
        
def delete_piece(piece, board):
    shape = piece.shape[piece.rotation]
    for i in range(0, len(shape)):
        for j in range(0, len(shape[i])):
            if(shape[i][j] == 1):
                board[piece.y + i][piece.x + j] = 0
    return board  

def render_board(board, surface):
    for i in range(1, len(board)-1):
        for j in range(1, len(board[i])-1):
            if(board[i][j] > 0):
                pygame.draw.rect(surface, SHAPES_COLORS[board[i][j] - 1], (TOP_LEFT_X + (j - 1)*BLOCK_SIZE, 
                                 TOP_LEFT_Y + (i - 1)*BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE), 0)
            else:
                pygame.draw.rect(surface, (0,0,0), (TOP_LEFT_X + (j - 1)*BLOCK_SIZE, 
                                 TOP_LEFT_Y + (i - 1)*BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE), 0)

def check_collision(board, piece, new_width, new_height):
    board = delete_piece(piece, copy.deepcopy(board))
    shape = piece.shape[piece.rotation]
    for i in range(0, len(shape)):
        for j in range(0, len(shape[i])):
            if(shape[i][j] == 1 and board[new_height + i][new_width + j] > 0):
                return True
    return False

def check_collision_rotate_cw(board, piece):
    board = delete_piece(piece, copy.deepcopy(board))
    piece_temp = copy.deepcopy(piece)
    piece_temp.rotation = 0 if piece_temp.rotation == 3 else piece_temp.rotation + 1

    shape = piece_temp.shape[piece_temp.rotation]
    for i in range(0, len(shape)):
        for j in range(0, len(shape[i])):
            if(shape[i][j] == 1 and board[piece_temp.y + i][piece_temp.x + j] > 0):
                return True
    return False

def check_collision_rotate_ccw(board, piece):
    board = delete_piece(piece, copy.deepcopy(board))
    piece_temp = copy.deepcopy(piece)
    piece_temp.rotation = 3 if piece_temp.rotation == 0 else piece_temp.rotation - 1

    shape = piece_temp.shape[piece_temp.rotation]
    for i in range(0, len(shape)):
        for j in range(0, len(shape[i])):
            if(shape[i][j] == 1 and board[piece_temp.y + i][piece_temp.x + j] > 0):
                return True
    return False

def remove_lines(board):
    lines_done_ids = []
    for i in range(1, len(board)-1):
        line_done = True
        for j in range(1, len(board[i])-1):
            if(board[i][j] == 0):
                line_done = False
                break
        if(line_done):
            lines_done_ids.append(i)
    for i in lines_done_ids:
        for k in reversed(range(1, i + 1)):
            for j in range(1, len(board[i]) - 1):
                board[k][j] = board[k - 1][j]

    return len(lines_done_ids)

def check_lose(board):
    for j in range(1, len(board[0]) - 1):
        if(board[1][j] > 0):
            return True
    return False

def time_to_str(value):
    valueD = (((value/365)/24)/60)
    Days = int (valueD)

    valueH = (valueD-Days)*365
    Hours = int(valueH)

    valueM = (valueH - Hours)*24
    Minutes = int(valueM)

    valueS = (valueM - Minutes)*60
    Seconds = int(valueS)
    
    return str(Minutes) + ":" + str(Seconds)

def draw_grid(surface, board):
    _sx = TOP_LEFT_X
    _sy = TOP_LEFT_Y

    for i in range(0, len(board)-2):
        pygame.draw.line(surface, (128, 128, 128), (_sx, _sy + i*BLOCK_SIZE),
                         (_sx+PLAY_WIDTH, _sy+ i*BLOCK_SIZE))
        for j in range(0, len(board[i])-2):
            pygame.draw.line(surface, (128, 128, 128), (_sx + j*BLOCK_SIZE, _sy),
                             (_sx + j*BLOCK_SIZE, _sy + PLAY_HEIGHT))

def draw_playfield(surface, board):
    draw_grid(surface, board)
    pygame.draw.rect(surface, (30, 36, 96), (TOP_LEFT_X, TOP_LEFT_Y, PLAY_WIDTH, PLAY_HEIGHT), 5)

def draw_lines_removed(surface, background_color, lines_done = 0):
    font = pygame.font.SysFont('comicsans', 30)
    label = font.render('Lines done: ' + str(lines_done), 1, (255, 255, 255))

    _sx = TOP_LEFT_X + PLAY_WIDTH + 50
    _sy = int(TOP_LEFT_Y + PLAY_HEIGHT/2 - 100)

    pygame.draw.rect(surface, background_color, (_sx, _sy, 500, BLOCK_SIZE), 0)

    surface.blit(label, (_sx + 20, _sy))

def draw_time(surface, board, time, background_color):
    font = pygame.font.SysFont('comicsans', 30)
    label = font.render('Time: ' + time_to_str(time), 1, (255, 255, 255))

    _sx = TOP_LEFT_X + PLAY_WIDTH + 50
    _sy = int(TOP_LEFT_Y + PLAY_HEIGHT/2 - 100)

    pygame.draw.rect(surface, background_color, (_sx + 20, _sy + 40, 500, BLOCK_SIZE), 0)

    surface.blit(label, (_sx + 20, _sy + 40))

def draw_next_shape(surface, shape):
    _sx = TOP_LEFT_X + PLAY_WIDTH + 50
    _sy = int(TOP_LEFT_Y + PLAY_HEIGHT/2 - 250)
    format1 = shape[0]
    pygame.draw.rect(surface, (0, 0, 0), (_sx, _sy, BLOCK_SIZE * 4, BLOCK_SIZE * 4), 0)
    for i, line in enumerate(format1):
        row = list(line)
        for j, column in enumerate(row):
            if column == 1:
                pygame.draw.rect(surface, SHAPES_COLORS[SHAPES.index(shape)], (_sx + j*BLOCK_SIZE, _sy + i*BLOCK_SIZE,
                                                        BLOCK_SIZE, BLOCK_SIZE), 0)

def draw_next_shape_label(surface):
    font = pygame.font.SysFont('comicsans', 30)
    label = font.render('Next Shape', 1, (255, 255, 255))
    _sx = TOP_LEFT_X + PLAY_WIDTH + 50
    _sy = int(TOP_LEFT_Y + PLAY_HEIGHT/2 - 250)
    surface.blit(label, (_sx + 10, _sy - 30))

def draw_text_middle(surface, text, size, color):
    font = pygame.font.SysFont("comicsans", size, bold=True)
    label = font.render(text, 1, color)

    surface.blit(label, (int(TOP_LEFT_X + PLAY_WIDTH /2 - (label.get_width()/2)),
                         int(TOP_LEFT_Y + PLAY_HEIGHT/2 - label.get_height()/2)))

def main(window):
    run = True
    start_timer = time.time()
    clock = pygame.time.Clock()
    curr_piece = Piece(4, 0, random.choice(SHAPES))
    next_shape = random.choice(SHAPES)
    board = copy.deepcopy(BOARD_ORIGINAL)
    curr_height = 3
    curr_width = 5
    fall_time = 0
    fall_speed = 0.27
    lines_removed = 0
    lines_to_remove = 40
    game_started = False
    background_color = (41,115,184)

    window.fill(background_color)
    draw_time(window, board, time.time() - start_timer, background_color)
    draw_lines_removed(window, background_color)
    render_board(board, window)
    draw_playfield(window, board)

    draw_text_middle(window, 'Press space to play', 35, (255, 255, 255))

    pygame.display.update()
    draw_next_shape_label(window)

    while run:
        while not game_started:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.display.quit()
                if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                    game_started = True
                    clock = pygame.time.Clock()
                    lines_removed = 0
                    fall_time = 0
                    curr_piece = Piece(4, 0, random.choice(SHAPES))
                    next_shape = random.choice(SHAPES)
                    start_timer = time.time()
                    draw_lines_removed(window, background_color)
                    pygame.display.update()

        fall_time += clock.get_rawtime()
        clock.tick()
        draw_time(window, board, time.time() - start_timer, background_color)

        if fall_time/1000 > fall_speed:
            fall_time = 0
            curr_piece.y += 1

        board = add_piece(curr_piece, board)
        render_board(board, window)
        draw_playfield(window, board)
        draw_next_shape(window, next_shape)
        
        pygame.display.update()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_x:
                    if not check_collision_rotate_cw(board, curr_piece):
                        board = delete_piece(curr_piece, board)
                        if curr_piece.rotation == 3:
                            curr_piece.rotation = 0
                        else:
                            curr_piece.rotation += 1
                        board = add_piece(curr_piece, board)
                if event.key == pygame.K_z:
                    if not check_collision_rotate_ccw(board, curr_piece):
                        board = delete_piece(curr_piece, board)
                        if curr_piece.rotation == 0:
                            curr_piece.rotation = 3
                        else:
                            curr_piece.rotation -= 1
                        board = add_piece(curr_piece, board)
                if event.key == pygame.K_LEFT:
                    if not check_collision(board, curr_piece, curr_piece.x - 1, curr_piece.y):
                        board = delete_piece(curr_piece,board)
                        curr_piece.x -= 1
                        board = add_piece(curr_piece, board)
                if event.key == pygame.K_RIGHT:
                    if not check_collision(board, curr_piece, curr_piece.x + 1, curr_piece.y):
                        board = delete_piece(curr_piece,board)
                        curr_piece.x += 1
                        board = add_piece(curr_piece, board)
                if event.key == pygame.K_SPACE:
                    while not check_collision(board, curr_piece, curr_piece.x, curr_piece.y + 1):
                        board = delete_piece(curr_piece,board)
                        curr_piece.y += 1
                        board = add_piece(curr_piece, board)
                    lines_removed += remove_lines(board)
                    draw_lines_removed(window, background_color, lines_removed)
                    curr_piece = Piece(4, 0, next_shape)
                    next_shape = random.choice(SHAPES)
                    draw_next_shape(window, next_shape)
                    if(lines_removed > lines_to_remove - 1):
                        game_started = False
                        draw_playfield(window, board)
                        render_board(board, window)
                        draw_playfield(window, board)
                        draw_text_middle(window, 'You win!', 35, (255, 255, 255))
                        pygame.display.update()
                        board = copy.deepcopy(BOARD_ORIGINAL)
                    if(check_lose(board)):
                        game_started = False
                        draw_playfield(window, board)
                        render_board(board, window)
                        draw_playfield(window, board)
                        draw_text_middle(window, 'You lose!', 35, (255, 255, 255))
                        pygame.display.update()
                        board = copy.deepcopy(BOARD_ORIGINAL)

        if check_collision(board, curr_piece, curr_piece.x, curr_piece.y + 1):
            lines_removed += remove_lines(board)
            draw_lines_removed(window, background_color, lines_removed)
            curr_piece = Piece(4, 0, next_shape)
            next_shape = random.choice(SHAPES)
            draw_next_shape(window, next_shape)
            if(lines_removed > lines_to_remove - 1):
                game_started = False
                render_board(board, window)
                draw_playfield(window, board)
                draw_text_middle(window, 'You win!', 35, (255, 255, 255))
                pygame.display.update()
                board = copy.deepcopy(BOARD_ORIGINAL)
            if(check_lose(board)):
                game_started = False
                render_board(board, window)
                draw_playfield(window, board)
                draw_text_middle(window, 'You lose!', 35, (255, 255, 255))
                pygame.display.update()
                board = copy.deepcopy(BOARD_ORIGINAL)
        else:
            board = delete_piece(curr_piece, board)

    pygame.display.quit()

WIN = pygame.display.set_mode((GAME_WIDTH, GAME_HEIGHT))
pygame.display.set_caption('Tetris')
main(WIN)